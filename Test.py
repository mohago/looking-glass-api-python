'''
Created on 1 Jun 2015

@author: Dylan
'''
import unittest
from mohago.DataSet import DataSet
import math
import datetime
from datetime import timedelta
from distutils.core import setup


class Test(unittest.TestCase):


    def testName(self):
        '''
        '''
        
        dataset = DataSet('dylan.buckley@mohago.com', 'Capslock0')#'dylan.buckley@mohago.com', '****'
        dataset.set_name('sine4')
        t = datetime.datetime.now()
        dataset.set_parent_global_id('APIJMUYNL_20150602133315')
        for angle in range(20, 350):
            dataset.add_parameter('x', angle)
            dataset.add_parameter('y', math.sin(math.radians(angle)))
            dataset.add_parameter('t1', t)
            dataset.end_item()
            t = t + timedelta(days=1)

        globId = dataset.end_write()
        print globId
        
        #setup(name='foo',version='1.0',py_modules=['foo'])
        
        #FileUploader()._upload_csv('C:\\ProgramData\\Mohago\\Lgdf\\Book1.csv', DataSet.CLOUD_URL + '/datasets/csv', 'dylan.buckley@mohago.com', 'Capslock1')
        
        
        
        #FileUploader().upload('C:\\ProgramData\\Mohago\\Lgdf\\20150602133315_R6XHBP.lgmf', DataSet.CLOUD_URL + '/datasets/lgf', 'dylan.buckley@mohago.com', 'Capslock1')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()