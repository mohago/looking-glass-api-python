'''
Created on 25 May 2015

@author: Dylan
'''
import datetime
import random
import xml.etree.cElementTree as ET
import hashlib
import os, io
#from requests_toolbelt import MultipartEncoder
import requests
from enum import Enum
from datetime import datetime
from ctypes import CDLL, c_char_p

class DataSet(object):
    '''
    classdocs
    '''
    
    CHARS = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9' ]
    HEX_DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    NAME_LENGTH_ERROR = 'Name of parameter must be under 255 characters in length'
    CLOUD_URL = 'https://api.mohago.com/qa/rest'


    def __init__(self, *args, **kwargs):
        '''
        Constructor for non stream type Datasets
        '''
        self.name = 'unnamed_API_dataset'
        self.date = datetime.now()
        formattedDate = datetime.strftime(self.date, '%Y%m%d%H%M%S')
        self.selfGlobalId = 'API_' + self._create_token() + '_' + formattedDate
        self.project = 'local'
        self.task = 'API import'
        self.source = 'API_capture'
        self.user = 'API_local'
        self.firstWrite = True;
        self.isStreamType = False
        self.isStartStream = False
        self.isCloudType = False;
        self.parentGlobalId = ''
        self.finalWrite = False
        
        '''
        Stream type or Cloud type
        '''
        if len(args) == 1:
            if args[0] == True:
                self.isParent = True
                self.isStartStream = True
                self.parentGlobalId = self.selfGlobalId
                self.isStreamType = True
                self.streamIndex = 0
                self.streamCalled = False
                self.finalWrite = False
        
        
        elif len(args) == 2:
            self.isCloudType = True;
            self.cloudUserName = args[0]
            self.cloudPassword = args[1]
            
         
        '''
        Member variables
        '''   
        self.metaInteger = []
        self.metaDouble = []
        self.metaString = []
        self.metaDate = []
        
        self.parameterCollectionInteger = []
        self.parameterCollectionDouble = []
        self.parameterCollectionString = []
        self.parameterCollectionDate = []
        
        self.parameterNames = []
        self.allParams = []
        self.allMeta = []
        self.itemCount = 0
        self.parameterIndex = 0
        self.runningList = []
        self.filePath = ''
        self.parameterNameTypeList = []
        
        '''
        Config variables
        '''
        self.filePath = ''
        self.metaPath = ''
        
                
    
    def _create_token(self):
        '''
        Creates a random alphanumeric Token
        '''
        ret = ''
        for c in range(0, 6):
            ret += DataSet.CHARS[random.randrange(len(DataSet.CHARS))]
        return ret
    
    def attachMeta(self, name, value):
        '''
        Attach element of Meta info
        '''
        if len(name) > 255:
            raise LookingGlassException('Name of meta must be under 255 characters in length')
        
        if type(value) is int:
            self.metaInteger.append(Meta(name, value, LookingGlassType.integer))
        elif type(value) is float:
            self.metaDouble.append(Meta(name, value, LookingGlassType.double_precision))
        elif type(value) is datetime:
            self.metaDate.append(Meta(name, value, LookingGlassType.date_time))
        elif type(value) is str:
            self.metaString.append(Meta(name, value, LookingGlassType.string_value))
    
    
    def _clear_collections(self):
        for i in range(len(self.runningList)):
            self.runningList[i]._clear_values()
    
    def _create_file_name(self):
        formattedDate = datetime.strftime(self.date, '%Y%m%d%H%M%S%f')[:-2]
        ret = formattedDate + '_' + self._create_token() + '.lgdf'
        return ret
    
    def end_item(self):
        '''
        Call this to end the current item that parameters are being added to, and
        move to a new Item.
      
        '''
        for i in range(len(self.runningList)):
            if not self.runningList[i].newItemThisRound:
                self.runningList[i]._pad_value()
            self.runningList[i].newItemThisRound = False
        if self.itemCount > 100:
            self._periodic_write()
        else:
            self.itemCount = self.itemCount + 1
    
    def _write_lgmf(self):
        meta = ET.Element('Meta')
        
        self.allParams = sorted(self._get_all_params(), key=lambda pc: pc.parameterIndex)
        if len(self.allParams) > 0:
            parameterCollections = ET.SubElement(meta, 'ParameterCollections')
            for i in range(len(self.allParams)):
                paramCol = ET.SubElement(parameterCollections, 'ParamCol')
                paramCol.attrib['Name'] = self.allParams[i].p_name
                paramCol.attrib['Type'] = self.allParams[i].parameterType.name
                paramCol.attrib['Index'] = str(self.allParams[i].parameterIndex)
                
        if len(self.allMeta) > 0:
            for i in range(len(self.allMeta)):
                metaElement = ET.SubElement(meta, self.allMeta[i].parameterType.name)
                metaElement.attrib['Name'] = self.allMeta[i].p_name
                if LookingGlassType.date_time == self.allMeta[i].parameterType:
                    metaElement.attrib['Value'] = datetime.strftime(self.allMeta[i].p_value, '%Y%m%d%H%M%S%f')[:-2]
                else:
                    metaElement.attrib['Value'] = self.allMeta[i].p_value
        if not FileUtils.is_null_or_empty(self.name):
            dsElement = ET.SubElement(meta, 'DataSetName')
            dsElement.attrib['Value'] = self.name
                
        if not FileUtils.is_null_or_empty(self.date):
            dsElement = ET.SubElement(meta, 'DataSetDate')
            dsElement.attrib['Value'] = datetime.strftime(self.date, '%Y%m%d%H%M%S%f')[:-2]
                
        if not FileUtils.is_null_or_empty(self.project):
            dsElement = ET.SubElement(meta, 'DataSetProject')
            dsElement.attrib['Value'] = self.project
                
        if not FileUtils.is_null_or_empty(self.task):
            dsElement = ET.SubElement(meta, 'DataSetTask')
            dsElement.attrib['Value'] = self.task
                
        if not FileUtils.is_null_or_empty(self.source):
            dsElement = ET.SubElement(meta, 'DataSetSource')
            dsElement.attrib['Value'] = self.source
            
        if not FileUtils.is_null_or_empty(self.selfGlobalId):
            dsElement = ET.SubElement(meta, 'DataSetGlobalId')
            dsElement.attrib['Value'] = self.selfGlobalId
            
        if (self.isStartStream):
            dsElement = ET.SubElement(meta, 'DataSetStartStream')
            dsElement.attrib['Value'] = 'true'
            
        if(not self.isStartStream) and not FileUtils.is_null_or_empty(self.parentGlobalId):
            dsElement = ET.SubElement(meta, 'DataSetParent')
            dsElement.attrib['Value'] = self.parentGlobalId
            
        if (self.isStreamType):
            dsElement = ET.SubElement(meta, 'StreamIndex')
            dsElement.attrib['Value'] = str(self.streamIndex)
            
        dsElement = ET.SubElement(meta, 'Done')
        if(self.finalWrite):
            dsElement.text = '1'
        else:
            dsElement.text = '0'
            
        dsElement = ET.SubElement(meta, 'Hash').text = self._calculate_md5_hash(self.filePath)
        dsElement = ET.SubElement(meta, 'Imported').text = '0'
        metaPath = ''
        if self.isCloudType:
            metaPath = self.filePath.replace('lgdf' + os.sep, 'lgdf' + os.sep + 'Transfer' + os.sep).replace('.lgdf', '.lgmf')
        else:
            metaPath = self.filePath.replace('.lgdf', '.lgmf')
        ET.ElementTree(meta).write(metaPath)
        return metaPath
           
            
                
    def _calculate_md5_hash(self, path):
        m = hashlib.md5()
        with open(path, 'rb') as f:
            while True:
                buf = f.read(2 ** 20)
                if not buf:
                    break
                m.update(buf)
        return m.hexdigest()
    
    def _periodic_write(self):
        if self.firstWrite:
            self._create_new_lgdf()
            self.firstWrite = False
        self.runningList = sorted(self.runningList, key=lambda pc: pc.parameterIndex)
        paramCount = len(self.runningList)
        itemCount = self._get_max_param_collection_count()
        s = ''
        with io.open(self.filePath, 'ab') as lgdf:
            for i in range(itemCount):
                s = ''
                for paramCol in range(paramCount):
                    s += self.runningList[paramCol]._string_value_at(i)
                    s += ','
                s = s[:-1]
                s += '\r\n'
                lgdf.write(s)
        self._clear_collections()
    
    def _create_new_lgdf(self):
        self.filePath = FileUtils.get_folder_path() + os.sep + self._create_file_name()
        if not os.path.isfile(self.filePath):
            open(self.filePath, 'w').close()
            
    def stream(self):
        '''
        Can be called on stream-type Datasets. Pass True into the Dataset constructor for a stream-type Dataset
        '''
        self.streamCalled = True
        if not self.isStreamType and not self.isCloudType:
            raise LookingGlassException('Stream can only be called on Stream-type Datasets. Pass in a Boolean of value True to the Dataset constructor to create a Stream-type Dataset.')
        self._perdiodic_write()
        metaPath = self._write_lgmf()
        if self.isCloudType and not FileUtils.is_null_or_empty(self.filePath) and FileUploader()._upload(metaPath, DataSet.CLOUD_URL + '/datasets/lgf', self.cloudUserName, self.cloudPassword): #MultipartFileUploader()._upload(metaPath, DataSet.CLOUD_URL + '/datasets/lgf', self.cloudUserName, self.cloudPassword):
            os.remove(metaPath)
            os.remove(self.filePath)
        self._create_new_lgdf()
        formattedDate = datetime.strftime(self.date, '%Y%m%d%H%M%S')
        self.selfGlobalId = 'API_' + self._create_token() + '_' + formattedDate
        self.isStartStream = False
        if self.isStreamType:
            self.streamIndex += 1
        return self.filePath
        
    def end_write(self):
        '''
        This method should be called when all Data has been added to the Dataset to complete it and write it to file
        '''
        if self.isStreamType:
            self.finalWrite = True
            if not self.streamCalled:
                raise LookingGlassException('Stream() must be called at least once before calling EndWrite() on a Stream-type Dataset.')
        self._periodic_write()
        metaPath = self._write_lgmf()
        ret = ''
        if self.isCloudType and not FileUtils.is_file_empty(self.filePath) and FileUploader()._upload(metaPath, DataSet.CLOUD_URL + '/datasets/lgf', self.cloudUserName, self.cloudPassword): #MultipartFileUploader()._upload(metaPath, DataSet.CLOUD_URL + '/datasets/lgf', self.cloudUserName, self.cloudPassword):
            if not self.parentGlobalId:
                ret = self.selfGlobalId
            else:
                ret = self.parentGlobalId
            os.remove(metaPath)
            os.remove(self.filePath)
        else:
            ret = metaPath
        return ret
            
    
    def _does_param_already_exist(self, name, lgType):
            ret = False
            for i in range(len(self.parameterNameTypeList)):
                if(self.parameterNameTypeList[i].first == name) and (self.parameterNameTypeList[i].second == lgType):
                    ret = True
            return ret
        
    def _generate_param_string(self):
        output = ''
        self.allParams = sorted(self._get_all_params(), key=lambda pc: pc.parameterIndex)
        if len(self.allParams) > 0:
            for i in range(len(self.allParams)):
                output += self.allParams[i].p_name + ','
            output = output[:-1]
        return output
    
    def write_csv(self, csvPath):
        '''
        Write the Dataset to .csv format
        '''
        with open(csvPath, 'a') as csvFile:
            csvFile.write(self._generate_param_string() + '\n')
            with open(self.filePath, 'rb') as lgdf:
                for line in lgdf:
                    csvFile.write(line)  
                    
    def set_name(self, name):
        '''
        Set a name for the Dataset
        '''
        self.name = name
        
    def set_project(self, project):
        '''
        Set a project for the Dataset
        '''
        self.project = project
    
    def set_parent_global_id(self, globalId):
        '''
        Set the Global Id of this Dataset's parent. This can be used with cloud Datasets to add Data to a parent Dataset.
        '''
        if self.isCloudType:
            self.parentGlobalId = globalId
        else:
            raise LookingGlassException('This method is only related to cloud type Datasets. Pass in mohago.com cloud credentials to create a cloud-type Dataset.')
            
    def add_parameter(self, name, value):
        '''
        Add a parameter
        '''
        if len(name) > 255:
            raise LookingGlassException('Name of parameter must be under 255 characters in length')
        
        if type(value) is int:
            lgType = LookingGlassType.integer
            parameterCollection = self.parameterCollectionInteger
        elif type(value) is float:
            lgType = LookingGlassType.double_precision
            parameterCollection = self.parameterCollectionDouble
        elif type(value) is datetime:
            lgType = LookingGlassType.date_time
            parameterCollection = self.parameterCollectionDate
        elif type(value) is str:
            lgType = LookingGlassType.string_value
            parameterCollection = self.parameterCollectionString
        
        if self._does_param_already_exist(name, lgType):
            for param in parameterCollection:
                if param.p_name == name:
                    param.p_values.append(Pair(True, value))
                    param.newItemThisRound = True
        else:
            paramNew = ParameterCollection(name, lgType, self.parameterIndex)
            self.parameterIndex = self.parameterIndex + 1
            for i in range(self.itemCount):
                paramNew._pad_value()
            paramNew.p_values.append(Pair(True, value))
            paramNew.newItemThisRound = True
            parameterCollection.append(paramNew)
            self.runningList.append(paramNew)
            self.parameterNameTypeList.append(Pair(name, lgType))
        return
    
    def _get_max_indexed_param_collection_value(self):
        max = -1
        maxIndex = -1
        for param in self.allParams:
            maxIndex = param._get_max_index()
            if maxIndex > max:
                max = maxIndex
        return max
    
    def _get_max_param_collection_count(self):
        max = -1
        count = -1
        for param in self._get_all_params():
            count = param._get_count()
            if count > max:
                max = count
        return max
    
    def _get_all_params(self):
        paramArrayList = []
        for pdate in self.parameterCollectionDate:
            paramArrayList.append(pdate)
        for ps in self.parameterCollectionString:
            paramArrayList.append(ps)
        for pi in self.parameterCollectionInteger:
            paramArrayList.append(pi)
        for pd in self.parameterCollectionDouble:
            paramArrayList.append(pd)
        return paramArrayList

class ParameterCollection(object):
    '''
    classdocs
    '''


    def __init__(self, name, parameterType, index):
        '''
        Constructor
        '''
        self.p_name = name
        self.parameterType = parameterType
        self.parameterIndex = index
        self.p_values = []
        self.p_indexed_values = []
        self.newItemThisRound = False
    
    def _string_value_at(self, index):
        ret = ''
        pair = self.p_values[index]
        if pair.first and (pair.second is not None):
            if self.parameterType is LookingGlassType.date_time:
                ret = datetime.strftime(pair.second, '%Y%m%d%H%M%S%f')[:-2]
            else:
                ret = str(pair.second)
        
        return ret
    
    def _pad_value(self):
        self.p_values.append(Pair(True, None))
        
    def _clear_values(self):
        del self.p_values[:]
    
    def _get_max_index(self):
        if self.p_indexed_values is not None and len(self.p_indexed_values) > 0:
            i = 0
            maxIndex = -1
            max = 0
            for p in self.p_indexed_values:
                if p.first > max:
                    max = p.first
                    maxIndex = i
                i += 1
        return maxIndex
    
    def _get_count(self):
        ret = 0
        if self.p_values is not None:
            ret = len(self.p_values)
        return ret
    
class Meta(object):
    '''
    classdocs
    '''


    def __init__(self, name, value, lgType):
        '''
        Constructor
        '''
        self.p_name = name
        self.p_value = value
        self.parameterType = lgType
        
class Pair(object):
    '''
    classdocs
    '''


    def __init__(self, first, second):
        '''
        Constructor
        '''
        self.first = first
        self.second = second
    
class LookingGlassType(Enum):
    '''
    '''
    integer = 0
    double_precision = 1
    string_value = 2
    date_time  = 3
    
class LookingGlassException(Exception):
    '''
    '''
    def __init__(self, message):
        self.message = message
        


class FileUploader(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    def _upload(self, metaPath, uploadUrl, userName, password):
        ret = False
        try:
            #m = MultipartEncoder(
            #                     fields = {'lgmf': open(metaPath, 'rb'), 'lgdf': open(metaPath.replace('.lgmf','.lgdf'), 'rb')}
            #)
            files = {'lgmf': open(metaPath, 'rb'), 'lgdf': open(metaPath.replace('.lgmf','.lgdf'), 'rb')}
            headers = {'user_name': userName, 'password': password}#, 'Content-Type':m.content_type}
            r = requests.post(uploadUrl, files=files, headers= headers)
            if r.status_code == 200:
                ret = True
        except requests.exceptions.RequestException as e:
            ret = False
        return ret
    
    def _upload_csv(self, csvPath, uploadUrl, userName, password):
        ret = False
        try:
            files = {'csv': open(csvPath, 'rb')}
            headers = {'user_name': userName, 'password': password}
            r = requests.post(uploadUrl, files=files, headers= headers)
            if r.status_code == 200:
                ret = True
        except requests.exceptions.RequestException as e:
            ret = False
        return ret

class FileUtils(object):
    '''
    classdocs
    '''
    @staticmethod
    def is_file_empty(filePath):
        return os.stat(filePath).st_size == 0
    @staticmethod
    def is_null_or_empty(strToTest):
        return not strToTest
    @staticmethod
    def is_nix():
        osName = os.name
        return (osName.rfind('nix') >= 0 or osName.rfind('nux') >= 0 or osName.rfind('aix') >= 0 or osName.rfind('six') >= 0)
    @staticmethod
    def is_mac():
        return (os.name.rfind('mac') >= 0)
    @staticmethod
    def is_windows():
        return (os.name.rfind('win') >= 0 or os.name.rfind('nt') >= 0)
    @staticmethod
    def get_folder_path():
        folderPath = ''
        if FileUtils.is_nix():
            getenv = CDLL("libc.so.6").getenv
            getenv.restype = c_char_p
            userHomePath = getenv("HOME")
            folderPath = os.path.join(os.path.join(userHomePath, '.mohago'), 'Lgdf')
        elif FileUtils.is_mac():
            userHomePath = os.getenv('user.home')
            folderPath = userHomePath + os.sep + 'Library/Application Support/.mohago' + os.sep + 'Lgdf'
        elif FileUtils.is_windows():
            userHomePath = os.getenv('ALLUSERSPROFILE')
            folderPath = userHomePath + os.sep + 'Mohago' + os.sep + 'Lgdf'
        
        folderExists = os.path.isdir(folderPath)
        if (not folderExists) or FileUtils.is_null_or_empty(folderPath):
            os.makedirs(folderPath)
        
        return folderPath
    
