from setuptools import setup
setup(name='mohago',
      version='0.0.1',
      description='Looking Glass & Mohago Cloud Data Capture Api',
      author='Dylan Buckley',
      author_email='dylan.buckley@mohago.com',
      url='http://mohago.com',
      packages=['mohago'],
      license='http://mohago.com/downloads/license.html',
      install_requires=[
          'enum34'
      ],
      
     )